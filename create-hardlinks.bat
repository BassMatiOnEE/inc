@echo ---
@echo Run this after cloning the repo to a workstation.
@echo Replaces include files from the repo with hard links to local (maintained) source files.
@echo Now changes due do maintenance are automatically replicated to this repository.

pause

@echo Make locally maintained files read-only, then delete the others.

cd public
attrib +r colors.css
del *.*
attrib -r colors.css

@echo Delete directories

rd fnt /s /q
rd img /s /q
rd legal /s /q
rd navigation /s /q
rd toolar /s /q

@echo Create hard links for directories

mklink /j fnt ..\..\..\..\github\bassmati-on-code\inc\docs\fnt
mklink /j img ..\..\..\..\github\bassmati-on-code\inc\docs\img
mklink /j legal ..\..\..\..\github\bassmati-on-code\inc\docs\legal
mklink /j navigation ..\..\..\..\github\bassmati-on-code\inc\docs\navigation
mklink /j toolbar ..\..\..\..\github\bassmati-on-code\inc\docs\toolbar

@echo Collapsible folder is imported partially
@echo so hard links to used files are created

mkdir collapsible
mklink /h collapsible\collapsible-5.css ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\collapsible-5.css
mklink /h collapsible\collapsible-5.js ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\collapsible-5.js
mklink /h collapsible\orange-minus.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\orange-minus.svg
mklink /h collapsible\orange-plus.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\orange-plus.svg
mklink /h collapsible\orange-dot.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\orange-dot.svg
mklink /h collapsible\orange-minus-active.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\orange-minus-active.svg
mklink /h collapsible\orange-plus-active.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\orange-plus-active.svg
mklink /h collapsible\orange-dot-active.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\orange-dot-active.svg
mklink /h collapsible\table-rowgroups-1.css ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\table-rowgroups-1.css
mklink /h collapsible\table-rowgroups-1.js ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\table-rowgroups-1.js
mklink /h collapsible\white-minus.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\white-minus.svg
mklink /h collapsible\white-plus.svg ..\..\..\..\github\bassmati-on-code\inc\docs\collapsible\white-plus.svg

@echo Create hardlinks to used files

mklink /h counter.css ..\..\..\..\github\bassmati-on-code\inc\docs\counter.css
mklink /h footer-2.css ..\..\..\..\github\bassmati-on-code\inc\docs\footer-2.css
mklink /h footer-2.js ..\..\..\..\github\bassmati-on-code\inc\docs\footer-2.js
mklink /h header-2.css ..\..\..\..\github\bassmati-on-code\inc\docs\header-2.css
mklink /h header-2.js ..\..\..\..\github\bassmati-on-code\inc\docs\header-2.js
mklink /h loader-4.js ..\..\..\..\github\bassmati-on-code\inc\docs\loader-4.js
mklink /h page.css ..\..\..\..\github\bassmati-on-code\inc\docs\page.css
mklink /h page.js ..\..\..\..\github\bassmati-on-code\inc\docs\page.js
mklink /h picture-1.css ..\..\..\..\github\bassmati-on-code\inc\docs\picture-1.css
mklink /h sticky-table-headers-1.css ..\..\..\..\github\bassmati-on-code\inc\docs\sticky-table-headers-1.css
mklink /h svg-1.js ..\..\..\..\github\bassmati-on-code\inc\docs\svg-1.js
mklink /h svg-dom-helper-1.js ..\..\..\..\github\bassmati-on-code\inc\docs\svg-dom-helper-1.js

pause
s